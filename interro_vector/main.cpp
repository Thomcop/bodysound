#include <iostream>
#include "etudiants.h"
#include <vector>
#include <stdlib.h>

using namespace std;

Student::Student()
    :m_prenom(""), m_nom(), m_age(0)
{

}
Student::Student(string prenom, string nom, int age)
    :m_prenom(prenom), m_nom(nom), m_age(age)
{

}

string Student::getprenom () const
{
    return m_prenom;
}
string Student::getnom () const
{
    return m_nom;
}
int Student::getage () const
{
    return m_age;
}

void Student::affichage()
{
    cout<<"prenom : "<<getprenom()<<endl
        <<"nom : "<<getnom()<<endl
        <<"age : "<<getage()<<endl;
}
int main()
{
    string prenom, nom;
    int age;
    vector <Student> eleve;
    bool sortie=false;
    do
    {
        int choix;
        cout<<"choix : 1 etudiant 2 sortir"<<endl;
        cin>>choix;

        if(choix==1)
        {
            cout<< "entrer prenom : "<<endl;
            cin>>prenom;
            cout<< "entrer nom : "<<endl;
            cin>>nom;
            cout<< "entrer age : "<<endl;
            cin>>age;
            system("cls");
            Student eleve1(prenom,nom,age);
            eleve.push_back(eleve1);
            vector <Student> :: iterator it;
            for (it=eleve.begin(); it!=eleve.end(); it++)
            {
                //(*it).affichage();
                it->affichage();
            }
            system("pause");
            system("cls");
        }
        else
        {
            sortie=true;
        }
    }
    while(sortie!=true);
    return 0;
}
