#ifndef ETUDIANTS_H_INCLUDED
#define ETUDIANTS_H_INCLUDED

using namespace std;

class Student
{
private :
    string m_prenom;
    string m_nom;
    int m_age;
public :
    Student();
    Student(string prenom, string nom, int age);
    ///getter
    string getprenom () const;
    string getnom () const;
    int getage () const;

    void affichage();
};
#endif // ETUDIANTS_H_INCLUDED
