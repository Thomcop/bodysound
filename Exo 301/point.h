#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED

#include <iostream>

class Point
{
private:
    //attributs
    int m_posx;
    int m_posy;

public:
    //constructeur par defaut
    Point();

//destructeur
    ~Point();

//methodes
    void afficherCoordonnees();
//accesseurs (getters et setters)
    int getposx() const;
    int getposy() const;
    void setposx(int);
    void setposy(int);


};

#endif // POINT_H_INCLUDED

