#ifndef LIGNE_H_INCLUDED
#define LIGNE_H_INCLUDED

#include <iostream>
#include "point.h"

#endif // LIGNE_H_INCLUDED

class Ligne
{
private:
    //attributs
    int m_posx1;
    int m_posx2;
    int m_posy1;
    int m_posy2;

public:

//constructeur par defaut
    Ligne();

//destructeur
    ~Ligne();

//methodes
    void afficher();


//accesseurs (getters et setters)

    int getposx1();
    int getposx2();
    int getposy1();
    int getposy2();
    void setposx1(int);
    void setposx2(int);
    void setposy1(int);
    void setposy2(int);
};
