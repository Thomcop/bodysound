#include <iostream>
#include "point.h"
#include "ligne.h"
using namespace std;

#define nombrepoints 2

int main()
{
    int *posx = new int;
    int *posy = new int;

    Point point[nombrepoints]; //creation d'un tableau de points
    Ligne ligne[nombrepoints/2]; //creation d'un tableau de lignes

    for(int i=0; i<nombrepoints; i++)
    {
        point[i] = Point();

    cout << "Veuillez choisir les coordonnees de votre point numero " << i+1 <<endl << "Abscisse(x) : " << endl;
    cin >> *posx;
    cout << "Ordonnee(y) : " << endl;
    cin >> *posy;

    point[i].setposx(*posx);
    point[i].setposy(*posy);

    }

    for(int i=0; i<nombrepoints; i++) // afficher les coordonnees des points
    {
        point[i].afficherCoordonnees();
    }

    for(int i=0; i<nombrepoints-1; i++)
    {
        for(int j=0;j<nombrepoints/2;j++)
        {
            // On initialise les coordonn�es des lignes en fct des coordonn�es des point rentr�s par l'utilisateur
            ligne[j].setposx1(point[i].getposx());
            ligne[j].setposy1(point[i].getposy());
            ligne[j].setposx2(point[i+1].getposx());
            ligne[j].setposy2(point[i+1].getposy());

        }
    }

    for(int i=0; i<nombrepoints/2; i++)
    {
        ligne[i].afficher(); // affiche coordonnees (point 1 et 2) de chaque ligne
    }

    delete posx; // Suppression de la m�moire
    delete posy;
    return 0;
}
