#include "point.h"

using namespace std;

//constructeur par defaut
Point::Point()
    :m_posx(0), m_posy(0)
{

}

//destructeur
Point::~Point()
{

}

//methode
void Point::afficherCoordonnees()
    {
        cout << "Abscisse  du point: " << getposx() << endl;
        cout << "Ordonnee du point: " << getposy() << endl;
    }

//getters
int Point::getposx() const
    {
        return m_posx;
    }

int Point::getposy() const
    {
        return m_posy;
    }

//setters
void Point::setposx(int posx)
    {
        m_posx = posx;
    }

void Point::setposy(int posy)
    {
        m_posy = posy;
    }
