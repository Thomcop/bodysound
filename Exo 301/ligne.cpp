#include "ligne.h"
using namespace std;

//constructeur par defaut
Ligne::Ligne()
        :m_posx1(0), m_posx2(0), m_posy1(0), m_posy2(0)
{

}

//destructeur
Ligne::~Ligne()
{

}


void Ligne::afficher()
{
    cout<<"Les coordonnees du point 1 de la ligne sont : "<< getposx1() << ", " << getposy1() << endl;
    cout<<"Les coordonnees du point 2 de la ligne sont : "<< getposx2() << ", " << getposy2() << endl;

}

//getters
int Ligne::getposx1()
{
    return m_posx1;
}

int Ligne::getposy1()
{
    return m_posy1;
}

int Ligne::getposx2()
{
    return m_posx2;
}

int Ligne::getposy2()
{
    return m_posy2;
}

//setters
void Ligne::setposx1(int posx)
{
    m_posx1 = posx;
}

void Ligne::setposx2(int posx)
{
    m_posx2 = posx;
}

void Ligne::setposy1(int posy)
{
    m_posy1 = posy;
}

void Ligne::setposy2(int posy)
{
    m_posy2 = posy;
}
