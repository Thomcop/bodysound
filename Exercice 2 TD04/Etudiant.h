#ifndef ETUDIANT_H_INCLUDED
#define ETUDIANT_H_INCLUDED
#include<iostream>
///using namespace std;

class Etudiant
{
    private:

    std::string m_id;
    std::string m_nom;
    int m_anne;

    public:
        ///constructeur
        Etudiant();
        Etudiant(std::string _id, std::string _nom, int _anne);
        ~Etudiant();

        ///Acesseur
        std::string getId() const;
        void setId(std::string id);
        std::string getNom() const;
        void setNom(std::string nom);
        int getAnne() const;
        void setAnne(int anne);



};
#endif // ETUDIANT_H_INCLUDED
