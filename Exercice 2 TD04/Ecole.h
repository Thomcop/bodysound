#ifndef ECOLE_H_INCLUDED
#define ECOLE_H_INCLUDED
#include <vector>
#include "Etudiant.h"
class Ecole
{
private:
    std::string m_nomecole;
    std::vector<Etudiant> m_listetudiant;

public:
    ///constructeur
    Ecole();
    Ecole(std::string _nomecole,std::vector<Etudiant> _listetudiant);
    ~Ecole();
    ///methode
    std::vector<Etudiant> inscription(std::vector<Etudiant> _listetudiant);
    ///accesseur*
    std::string getNomecole() const;
    void setNomecole(std::string nomecole);
    std::vector<Etudiant>  getListe() const;
    void setListe(std::vector<Etudiant>  liste);
};
#endif // ECOLE_H_INCLUDED
