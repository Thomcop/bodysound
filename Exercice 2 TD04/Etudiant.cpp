#include "Etudiant.h"

Etudiant :: Etudiant (std::string _id,std::string _nom,int _anne)
          : m_id(_id), m_nom(_nom), m_anne(_anne)
          {}
Etudiant::~Etudiant(){}

std::string Etudiant:: getId() const
{
    return m_id;
}
std::string Etudiant:: getNom() const
{
    return m_nom;
}
int Etudiant:: getAnne() const
{
    return m_anne;
}
void Etudiant:: setId(std::string id)
{
    m_id=id;
}
void Etudiant:: setNom(std::string nom)
{
    m_nom=nom;
}
void Etudiant:: setAnne(int anne)
{
    m_anne=anne;
}
