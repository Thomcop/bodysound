#include <iostream>

using namespace std;

int main()
{
    double* toto=new double[11];
    double cpt(0);
    double moy(0);
    for (int i=0;i<10;i++)
    {
        cout<<"veuillez rentrer un entier pour la case "<<i<<" du tableau:"<<endl;
        cin>>toto[i];
        cpt=cpt+toto[i];
    }
    moy=cpt/10;
    toto[10]=moy;

    cout<<"voici votre tableau avec la moyenne des nombres a la case 11:"<<endl;
     for (int j=0;j<11;j++)
    {
        cout<<"|"<<toto[j]<<"|"<<endl;
    }
    delete[] toto;
    return 0;
}
