#ifndef headers TP3
#define headers TP3

#endif // headers TP3

class Rectangle
{
private:
    ///attributs
    int m_hauteur;
    int m_largeur;

public:
    ///contructeurs et destructeur
    Rectangle();
    Rectangle(int _largeur, int _longueur);
    ~Rectangle();
    ///m�thode
    int perimetre();
    int aire();
    void parametre();
    ///getter
    int gethauteur () const;
    int getlargeur () const;
    void sethauteur (int hauteur);
    void setlargeur (int largeur);
};
