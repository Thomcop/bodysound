#include <iostream>
#include "headers.h"

using namespace std;


///constructeur par defaut
Rectangle::Rectangle()
    :m_hauteur(0), m_largeur(0)
{

}

///contructeur surchargé
Rectangle::Rectangle(int hauteur, int largeur)
    :m_hauteur(hauteur), m_largeur(largeur)
{

}

///destructeur
Rectangle::~Rectangle()
{

}

///getter
int Rectangle::gethauteur () const
{
    return m_hauteur;
}

int Rectangle::getlargeur () const
{
    return m_largeur;
}
///setters
void Rectangle::sethauteur (int hauteur)
{
    m_hauteur=hauteur;
}
void Rectangle::setlargeur (int largeur)
{
    m_largeur=largeur;
}

///méthodes
int Rectangle::perimetre()
{
    return ((gethauteur()+getlargeur())*2);
}
int Rectangle::aire()
{
    return (gethauteur()*getlargeur());
}
void Rectangle::parametre()
{
    cout << "la hauteur est : "<<gethauteur()<<endl
         << "la largeur est : "<<getlargeur()<<endl
         << "le perimetre est : "<<perimetre()<<endl
         << "l'aire est : "<<aire()<<endl;
}

int main()
{
    int hauteur, largeur;
    Rectangle rectangle1 (0,0);///hauteur et largeur
    cout << "entrer une hauteur"<<endl;
    cin>>hauteur;
    rectangle1.sethauteur(hauteur);
    cout << "entrer une largeur"<<endl;
    cin>>largeur;
    rectangle1.setlargeur(largeur);
    rectangle1.parametre();
    return 0;
}
